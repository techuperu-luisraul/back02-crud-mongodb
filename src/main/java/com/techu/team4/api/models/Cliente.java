package com.techu.team4.api.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "clientes")
@JsonPropertyOrder({"id", "nombre", "apellidos"})
public class Cliente implements Serializable {
    @Id
    @NotNull
    private String id;
    @NotNull
    private String nombres;
    @NotNull
    private String apellidos;

    public Cliente() {}

    public Cliente(String id, String nombre, String apellidos)
    {
        this.setId(id);
        this.setNombres(nombre);
        this.setApellidos(apellidos);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
}
